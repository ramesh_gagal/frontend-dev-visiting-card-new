import React from 'react'
import { Button, Card, CardHeader, CardTitle } from 'react-bootstrap'
import { FaPlus } from "react-icons/fa";
import { useLocation, useNavigate, useParams } from 'react-router';
import { FaLongArrowAltLeft } from "react-icons/fa";

const Header = () => {
    const id = useLocation()
    const navigate = useNavigate()
    console.log(id.pathname, "dj")
    return (
        <div className='navbar navbar-expand-xl navbar-light bg-light mt-4 rounded-3'>
            <div className='d-flex justify-content-between align-items-center w-100 px-4  m-2'>
                <p className='h3  mb-0 '>{id?.pathname == '/' ? "Card Listing" :  "Add card details" }</p>
                <div className=''>
                    {id?.pathname == "/" ?
                        <Button onClick={()=> navigate('/add/card')}>    
                            <FaPlus className='me-2' /> Add Card
                        </Button>
                        :
                        <Button onClick={()=>  navigate(-1)}>
                            <FaLongArrowAltLeft className='me-2' /> Back
                        </Button> }            
                </div>
            </div>
        </div>
    )
}

export default Header