import React from 'react'
import BCards from '../Components/BCards'
import { Row } from 'react-bootstrap'
import Header from './Header'
import { Outlet } from 'react-router'

const Layout = () => {
  return (
    <>
     <div className='container-fluid'>
      <div className='row'>
        <div className='col-12'>
      <Header />
      <Outlet />
      </div>
      </div>
      </div>
    </>

  )
}

export default Layout