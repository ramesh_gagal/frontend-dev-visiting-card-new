import React, { useState } from 'react'
import "./ImageSwiper.css"
import data from "./carouselData.json";
import { BsArrowLeftCircleFill, BsArrowRightCircleFill } from "react-icons/bs"

const ImageSwiper = ({images}) => {
    const [slide, setSlide] = useState(0)
    const nextSlide = () => {
        setSlide(slide === images.length - 1 ? 0 : slide + 1)
        console.log(slide, "length")

    }
    const prevSlide = () => {
        setSlide(slide === 0 ? images.length - 1 : slide - 1)
        console.log(slide, "length")

    }
    return (
        <div className='app'>
            <BsArrowLeftCircleFill className='arrow arrow-left' onClick={prevSlide} />
            {images?.map((item, index) => (
                <img src={item} alt="" key={index} className={slide === index ? "slide" : "slide-hidden"} />
            ))}
            <BsArrowRightCircleFill className='arrow arrow-right' onClick={nextSlide} />
            <span className='indicators'>
                {images?.map((_, idx) => {
                    return <button key={idx} onClick={null} className={slide === idx ? "indicator" : "indicator indicator-inactive"}></button>
                })}
            </span>
        </div>
    );
}


export default ImageSwiper