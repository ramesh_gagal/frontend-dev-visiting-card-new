import React, { useEffect, useState } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { MdEmail } from "react-icons/md";
import { FaPhoneAlt } from "react-icons/fa";
import { IoLogoLinkedin } from "react-icons/io5";
import ImageSwiper from '../Utils/ImageSwiper';
import axios from 'axios';

const BusinessCard = () => {
  const [state, setState] = useState([]);

  const ApiCall = async () => {
    try {
      const response = await axios.get("https://visiting-card-backend.onrender.com/api/v1/card/list");
      setState(response?.data?.data);
      console.log(response, "response");
      console.log(state, "staaaaaaaaaaaaaaa");
    } catch (e) {
      console.log(e, "error");
    }
  };

  useEffect(() => {
    ApiCall();
  }, []);

  return (
    <Row xs={1} md={2} lg={4} className="g-4">
      {state?.map((i, index) => (
        <Col key={index}>
          <Card className="bg-light">
            <Card.Header className='m-0 p-0'>
              <ImageSwiper images={i?.images} />
            </Card.Header>
            <Card.Body>
              <div className="info text-center">
                <Card.Title>{i?.name}</Card.Title>
                <Card.Text>{i?.about}</Card.Text>
                <Card.Text className='d-flex align-items-center justify-content-center'>
                  <div className="d-flex align-items-center justify-content-center">
                    <MdEmail size={18} className="email-icon bg-danger rounded-circle text-white me-2" />
                    <Card.Text>{i?.email}</Card.Text>
                  </div>
                </Card.Text>
                <Card.Text>
                  <div className="d-flex align-items-center justify-content-center">
                    <FaPhoneAlt size={18} className="email-icon bg-danger rounded-circle text-white me-2" />
                    <Card.Text>{i?.phone}</Card.Text>
                  </div>
                </Card.Text>
                <Card.Text>
                  <div className="d-flex align-items-center justify-content-center">
                    <IoLogoLinkedin size={18} className="email-icon bg-danger rounded-circle text-white me-2" />
                    <Card.Text>{i?.linkedin}</Card.Text>
                  </div>
                </Card.Text>
              </div>
            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default BusinessCard;
